module GetProxy

  URL = "http://www.getproxy.jp/"

  def self.get
    page = HTTParty.get(URL)
    html = Nokogiri::HTML(page.body)

    tr = html.css("#mytable tr")[1..-1]
    values = tr.map do |line|
      td = line.css("td")
      matched_address = td[0].css("strong").first.content
        .match(/^((?:\d{,3})(?:\.\d{,3}){3})\:(\d{,5})$/)
      values = {
        ip_address: matched_address[1],
        port:       matched_address[2],
        country:    td[1].content
      }
    end
  end

end
