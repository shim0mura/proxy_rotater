class Proxy
  include HTTParty
  default_timeout 4

  ROUND_NUM = 2
  TEST_REQUEST_URL = "http://www.google.co.jp"
  BENCHMARK_TIMES = 3

  attr_reader :timeout, :requested_at, :last_response_time

  def initialize(options)
    options.merge!(
      requested_at:       [],
      response_time:      nil,
      last_response_time: nil,
      checked_at:         nil,
      timeout:            false,
      return_at:          nil
    )
    options.keys.each do |key|
      instance_variable_set("@#{key}", options[key])
    end

    self.class.http_proxy(@ip_address, @port)
    benchmark
  end

  def get_url(url)
    start = Time.now
    # TODO: redirect too deepの時にどうにかする
    begin
      res = self.class.get(url)
    rescue Errno::ETIMEDOUT, Errno::ECONNRESET, EOFError, Errno::ECONNREFUSED, Errno::EHOSTUNREACH => e
      @timeout = true
      return nil
    rescue Net::ReadTimeout, Net::OpenTimeout => e
      @timeout = true
      return nil
    end
    finish = Time.now
    @requested_at << finish.to_f
    @last_response_time = (finish.to_f - start.to_f).round(ROUND_NUM)
    return res
  end

  def benchmark
    urls = BENCHMARK_TIMES.times.map{|i| TEST_REQUEST_URL}
    response_times = Parallel.map(urls, in_threads: BENCHMARK_TIMES) do |url|
      start = Time.now.to_f
      res = get_url(TEST_REQUEST_URL)
      finish = Time.now.to_f
      res.nil? ? nil : (finish - start).round(ROUND_NUM)
    end

    # TODO: 微妙なのでリファクタリングする
    # sliceとかまとめられそう
    # ifの中はresponse_timeどうにかしたほうがいいのでは？
    if response_times.include?(nil)
      reset_size = @requested_at.size
      @requested_at.slice!(-1 * reset_size, reset_size)
      return
    end

    @response_time = (response_times.inject(0){|v,s|v+s}/response_times.size).round(ROUND_NUM)
    @checked_at = Time.now

    # ベンチマークの結果はused_rateとrequested_atに影響しないようにする
    @requested_at.slice!(-1 * BENCHMARK_TIMES, BENCHMARK_TIMES)
  end

  def get_request_intervals
    diff = []
    @requested_at.each_with_index do |num, i|
      next if i == 0
      diff << @requested_at[i] - @requested_at[i-1]
    end

    return diff
  end

end
